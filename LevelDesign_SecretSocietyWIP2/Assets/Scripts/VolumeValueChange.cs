﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeValueChange : MonoBehaviour {
    private AudioSource mainMenuMusic;

    private float musicVolume = 1f;


	// Use this for initialization
	void Start () {
        mainMenuMusic = GetComponent<AudioSource>();
		
	}
	
	// Update is called once per frame
	void Update () {
        mainMenuMusic.volume = musicVolume;
	}

    public void SetVolume(float Vol)
    {
        musicVolume = Vol;
    }
}
