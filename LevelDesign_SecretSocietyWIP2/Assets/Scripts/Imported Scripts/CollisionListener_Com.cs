﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionListener_Com : MonoBehaviour
{

    public bool collidedWithPlayer = false;

    void OnTriggerEnter(Collider col)
    {
        
        if (col.gameObject.tag == "Player")
        {
            collidedWithPlayer = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            collidedWithPlayer = false;
        }
    }

}