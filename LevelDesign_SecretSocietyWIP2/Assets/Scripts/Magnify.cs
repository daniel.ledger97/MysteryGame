﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnify : MonoBehaviour {

    public Animator animator;
    public GameObject zoomOverlay;
    public GameObject magnifyCamera;
    public Camera maincamera;

    public float MagnifyFOV = 15f;
    public float normalFOV;

    private bool IsZoomed = false;

    private void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            IsZoomed = !IsZoomed;
            animator.SetBool("Zoomed", IsZoomed);
            if (IsZoomed)
                StartCoroutine(OnZoomed());
            else
                OnUnZoomed();

            
        }
    }
    void OnUnZoomed()
    {
        zoomOverlay.SetActive(false);
        magnifyCamera.SetActive(true);
        maincamera.fieldOfView = normalFOV;
    }
    IEnumerator OnZoomed()
    {
        yield return new WaitForSeconds(.15f);

        zoomOverlay.SetActive(true);
        magnifyCamera.SetActive(false);

        normalFOV = maincamera.fieldOfView;
        maincamera.fieldOfView = MagnifyFOV;
    }
}
  
