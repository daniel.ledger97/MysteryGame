﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public static bool GameISPaused = false;
    public GameObject pauseMenuUI;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameISPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
	}
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameISPaused = false;
        Camera.main.GetComponent<CharacterController>().enabled = true;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameISPaused = true;
        //Camera.main.GetComponent<CharacterController>().enabled = false;
    }    
    
    public void LoadMenu()
    {
        Debug.Log("loading menu ");
    }
    public void QuitGame()
    {
        Debug.Log(" quitting");
        Application.Quit();
    }
}
